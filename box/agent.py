import sys

from iotAmak.agent.async_agent import AsyncAgent


class BoxAgent(AsyncAgent):

    def __init__(self, arguments: str):
        super().__init__(arguments)

    def on_act(self) -> None:
        self.publish("box", "")


if __name__ == '__main__':
    s = BoxAgent(str(sys.argv[1]))
    s.run()
