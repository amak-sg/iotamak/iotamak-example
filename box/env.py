import sys

from iotAmak.env.async_env import AsyncEnvironment

from box import Box


class BoxEnv(AsyncEnvironment):

    def __init__(self, arguments, nbr_box):
        self.nbr_box = nbr_box
        self.boxs = [Box() for _ in range(nbr_box)]
        super().__init__(arguments)
        for i in range(nbr_box):
            self.subscribe("agent/"+str(i)+"/box", self.box_action)

    def box_action(self, client, userdata, message):
        agent_id = int(str(message.topic).split("/")[1])
        self.boxs[agent_id].add()

    def behaviour(self) -> None:
        for i in range(self.nbr_box):
            print("Box : ",i," -> ",self.boxs[i].x)

if __name__ == '__main__':
    s = BoxEnv(str(sys.argv[1]), 4)
    s.run()