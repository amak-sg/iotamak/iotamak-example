import sys
from math import sqrt

from iotAmak.amas.amas import Amas


class AntAmas(Amas):

    def __init__(self, arguments: str, nbr_agent):
        self.agent_to_create = nbr_agent
        super().__init__(arguments)

    def on_initial_agents_creation(self):
        for _ in range(self.agent_to_create):
            self.add_agent()

    def is_neighbour(self, id_a, id_b):
        if id_a == id_b:
            return False
        if self.agents_metric[id_a] == {}:
            return False
        if self.agents_metric[id_b] == {}:
            return False

        x = self.agents_metric[id_a].get("x") - self.agents_metric[id_b].get("x")
        y = self.agents_metric[id_a].get("y") - self.agents_metric[id_b].get("y")

        dist = sqrt(x * x + y * y)
        return dist < 1500

    def on_cycle_begin(self) -> None:

        for i in range(self.next_id):
            for j in range(i, self.next_id):
                cond = self.is_neighbour(i, j)
                if cond:
                    self.agent_neighbour(i, j)
                    self.agent_neighbour(j, i)


if __name__ == '__main__':
    s = AntAmas(str(sys.argv[1]), 2)
    s.run()
