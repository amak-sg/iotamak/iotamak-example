import random
import sys

from iotAmak.agent.communicating_agent import CommunicatingAgent


class Ant(CommunicatingAgent):
    int_to_color = {
        0: "#000000",
        1: "#ff0000",
        2: "#00ff00",
        3: "#0000ff",
    }
    color_to_int = {
        "#000000": 0,
        "#ff0000": 1,
        "#00ff00": 2,
        "#0000ff": 3,
    }

    def __init__(self, arguments: str):
        self.x = 0
        self.y = 0
        self.color = "#000000"

        self.other_color = [0 for _ in range(4)]

        super().__init__(arguments)

    def on_initialization(self) -> None:
        pass

    def on_cycle_begin(self) -> None:
        pass

    def on_perceive(self) -> None:
        self.neighbors = self.next_neighbors
        self.next_neighbors = []

        self.other_color = [0 for _ in range(4)]
        for mail in self.mailbox:
            self.other_color[Ant.color_to_int.get(mail.payload.get("color"))] += 1
        self.mailbox = []

    def on_decide(self) -> None:
        pass

    def on_act(self) -> None:
        self.x += random.randint(-5, +5)
        self.y += random.randint(-5, +5)
        if self.x < 0:
            self.x = 0
        if self.y < 0:
            self.y = 0

        # count color
        index_max = self.other_color.index(max(self.other_color[1:]))
        if self.other_color[index_max] != 0:
            self.color = Ant.int_to_color.get(self.other_color[index_max])
        # low chance to mutate
        if random.randint(0, 1000) < 10:
            self.color = Ant.int_to_color.get(random.randint(0, 3))

    def on_cycle_end(self) -> None:
        for n in self.neighbors:
            self.send_mail(n.get("id"), {"color": self.color})

    def to_canvas(self) -> dict:
        return {
            "id": self.id,
            "x": self.x,
            "y": self.y,
            "color": self.color,
            "cycle": self.nbr_cycle
        }

    def send_metric(self):
        metric = super(Ant, self).send_metric()

        metric["x"] = self.x
        metric["y"] = self.y
        return metric


if __name__ == '__main__':
    a = Ant(str(sys.argv[1]))
    a.run()
