class Fork:

    def __init__(self, identifier):

        self.identifier = identifier

        # 0: dirty, 1: clean
        self.state = 0

        self.taken_by = -1

    def to_msg(self):
        return {
                "state": self.state,
                "taken_by": self.taken_by
            }