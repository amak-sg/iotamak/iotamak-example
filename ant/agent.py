import random
import sys

from iotAmak.agent.agent import Agent


class Ant(Agent):
    int_to_color = {
        0: "#000000",
        1: "#ff0000",
        2: "#00ff00",
        3: "#0000ff",
    }
    color_to_int = {
        "#000000": 0,
        "#ff0000": 1,
        "#00ff00": 2,
        "#0000ff": 3,
    }

    def __init__(self, arguments: str):
        self.x = 0
        self.y = 0
        self.color = "#000000"
        super().__init__(arguments)

    def on_initialization(self) -> None:
        pass

    def on_cycle_begin(self) -> None:
        pass

    def on_perceive(self) -> None:
        self.neighbors = self.next_neighbors
        self.next_neighbors = []

    def on_decide(self) -> None:
        pass

    def on_act(self) -> None:
        self.x += random.randint(-5, +5)
        self.y += random.randint(-5, +5)
        if self.x < 0:
            self.x = 0
        if self.y < 0:
            self.y = 0

        # count color
        color = [0 for _ in range(4)]
        for ant in self.next_neighbors:
            color[Ant.color_to_int.get(ant.get("color"))] += 1
        # set color
        if color.index(max(color)) != 0:
            self.color = Ant.int_to_color.get(color[color.index(max(color))])
        # low chance to mutate
        if random.randint(0, 1000) < 10:
            self.color = Ant.int_to_color.get(random.randint(0, 3))

    def on_cycle_end(self) -> None:
        pass

    def send_metric(self):
        metric = super(Ant, self).send_metric()

        metric["x"] = self.x
        metric["y"] = self.y
        metric["color"] = self.color
        return metric

    def to_canvas(self) -> dict:
        return {
            "id": self.id,
            "x": self.x,
            "y": self.y,
            "color": self.color,
            "cycle": self.nbr_cycle
        }

if __name__ == '__main__':
    a = Ant(str(sys.argv[1]))
    a.run()
