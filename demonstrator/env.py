import sys
from iotAmak.env.environment import Environment


class IotEnv(Environment):

    def __init__(self, arguments):
        super().__init__(arguments)


if __name__ == '__main__':
    s = IotEnv(str(sys.argv[1]))
    s.run()