import random
import sys

from iotAmak.agent.communicating_agent import CommunicatingAgent


class IotAgent(CommunicatingAgent):

    def __init__(self, arguments: str, nbr_agent: int):
        self.nbr_agent: int = nbr_agent
        self.values = []
        self.asked = []
        self.nei = []
        super().__init__(arguments)

    def on_initialization(self) -> None:
        self.operator = random.choice(["+", "-", "*", "/"])
        self.value = random.randint(1, 10)

    def on_cycle_begin(self) -> None:
        pass

    def on_perceive(self) -> None:

        while len(self.nei) < 2:
            self.get_new_nei()

        self.values = []
        for mail in self.mailbox:
            if mail.payload == "AskValue":
                self.asked.append(int(mail.sender_id))
            elif mail.payload == "Remove":
                self.asked.remove(int(mail.sender_id))
            else:
                self.values.append(int(mail.payload))
        self.mailbox = []

        if random.randint(0, 100) == 1:
            self.nei = [self.nei[0]]

    def on_decide(self) -> None:
        pass

    def on_act(self) -> None:
        if self.value == 0:
            self.value = random.randint(1, 10)

        if self.value > 1000000:
            self.value = random.randint(1, 10)

        if len(self.values) == 2:
            if self.operator == "+":
                self.value = self.values[0] + self.values[1]
            elif self.operator == "-":
                self.value = self.values[0] - self.values[1]
            elif self.operator == "*":
                self.value = self.values[0] * self.values[1]
            elif self.operator == "/":
                if self.values[1] == 0:
                    self.nei = [self.nei[0]]
                else:
                    self.value = self.values[0] / self.values[1]

    def on_cycle_end(self) -> None:
        for n in self.asked:
            self.send_mail(n, self.value)
        self.log(" Value : " + str(self.value) +
                 " OP : " + self.operator
                 )

    def get_new_nei(self):
        nei = self.id
        while self.id == nei:
            nei = random.randint(0, self.nbr_agent - 1)
        self.nei = [nei] + self.nei
        self.send_mail(nei, "AskValue")

    def remove_nei(self):
        self.send_mail(self.nei[1], "Remove")
        self.nei = [self.nei[0]]


if __name__ == '__main__':
    a = IotAgent(str(sys.argv[1]), int(sys.argv[2]))
    a.run()
